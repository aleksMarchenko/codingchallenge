package cisco.java.challenge.countWords;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileReader {

    private String filePath;

    public FileReader(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    protected String readFile() throws IOException {

        StringBuilder s = new StringBuilder();

        Scanner readFile = new Scanner(new File(getFilePath()));
        while ((readFile.hasNext())) {
            s.append(readFile.nextLine().replaceAll("\\n+", "")).append(" ");
        }
        readFile.close();
        return String.valueOf(s).trim().replaceAll(" +", " ");
    }


    protected List<String> stringToList(String string) {
        List<String> strings = new ArrayList<>();

        Pattern pattern =
                Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS | Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            strings.add(matcher.group());
        }
        return strings;
    }

    protected void countWords(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        list.forEach(a ->
                map.compute(a, (w, prev) -> prev != null ? prev + 1 : 1)
        );

        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println("For this input file, the word " + entry.getKey() + " occured " + entry.getValue() + " times");
        }
    }
}
