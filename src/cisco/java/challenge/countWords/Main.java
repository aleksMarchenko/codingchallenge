package cisco.java.challenge.countWords;

import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        String filePath = "./src/cisco/java/challenge/countWords/parser.txt";

        FileReader reader = new FileReader(filePath);

        String s = reader.readFile();
        List<String> strings = reader.stringToList(s);
        reader.countWords(strings);
    }
}
