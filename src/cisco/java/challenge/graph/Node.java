package cisco.java.challenge.graph;

import ciscoTest.java.challenge.GNode;

import java.util.List;
import java.util.Objects;

public class Node implements GNode, Comparable<Node> {

    private int id;
    private String name;
    private List<Node> neighbors;


    public void setNeighbors(List<Node> neighbors) {
        this.neighbors = neighbors;
    }

    public Node(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Node(String name) {
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public GNode[] getChildren() {
        GNode[] nodes = new GNode[neighbors.size()];
        if (nodes.length != 0) {
            for (int i = 0; i < neighbors.size(); i++) {
                nodes[i] = neighbors.get(i);
            }
        } else {
            nodes = new GNode[0];
        }
        return nodes;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return id == node.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Node o) {
        return o.getId().compareTo(id);
    }
}
