package cisco.java.challenge.graph;

import java.util.*;

public class Graph {

    private HashMap<Node, List<Node>> adjacencyMap;

    public Graph() {
        adjacencyMap = new HashMap<>();
    }

    public void addEdgeHelper(Node a, Node b) {

        List<Node> tmp;

        tmp = (adjacencyMap.get(a));

        if (tmp != null) {
            tmp.remove(b);
        } else tmp = new LinkedList<>();
        tmp.add(b);
        adjacencyMap.put(a, tmp);
    }

    public void addEdge(Node source, Node destination) {
        addEdgeHelper(source, destination);
    }

    public ArrayList<Node> walkGraph() {
        ArrayList<Node> list = new ArrayList();
        for (Node node : adjacencyMap.keySet()) {
            list.add(node);
        }
        return list;
    }

    public List<Node> getVertex(String s) {
        return adjacencyMap.get(new Node(s));
    }


    Set<String> depthFirstTraversal(Graph graph, String root) {
        Set<String> visited = new LinkedHashSet<>();
        Stack<String> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            String vertex = stack.pop();
            if (!visited.contains(vertex)) {
                visited.add(vertex);
                for (Node v : graph.getVertex(vertex)) {
                    stack.push(v.getName());
                }
            }
        }
        return visited;
    }
}
