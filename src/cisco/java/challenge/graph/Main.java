package cisco.java.challenge.graph;

import java.util.ArrayList;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Graph graph = new Graph();

        Node a = new Node(1, "A");
        Node b = new Node(2, "B");
        Node c = new Node(3, "C");
        Node d = new Node(4, "D");
        Node e = new Node(5, "E");
        Node f = new Node(6, "F");
        Node g = new Node(7, "G");
        Node h = new Node(8, "H");
        Node i = new Node(9, "I");

        graph.addEdge(a, b);
        graph.addEdge(a, c);
        graph.addEdge(a, d);
        graph.addEdge(b, a);
        graph.addEdge(b, e);
        graph.addEdge(b, f);
        graph.addEdge(c, a);
        graph.addEdge(c, g);
        graph.addEdge(c, h);
        graph.addEdge(c, i);
        graph.addEdge(d, a);
        graph.addEdge(f, b);
        graph.addEdge(e, b);
        graph.addEdge(g, c);
        graph.addEdge(h, c);
        graph.addEdge(i, c);

        ArrayList list = graph.walkGraph();
        for (int k = 0; k < list.size(); k++) {
            System.out.println(list.get(k));
        }

        Set<String> a1 = graph.depthFirstTraversal(graph, "A");
    }
}
