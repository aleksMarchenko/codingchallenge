package cisco.java.challenge.graph;

public interface GNode {
    String getName();

    GNode[] getChildren();
}
