package cisco.java.challenge.countWords;

import org.junit.Before;
import org.junit.ComparisonFailure;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileReaderTest {
    protected FileReader reader = new FileReader("./src/cisco/java/challenge/countWords/parser.txt");
    protected final String stringTest = "Foo Foo dsds aa";
    protected final List<String> listStringTest = Arrays.asList("Foo", "Foo", "dsds", "aa");


    @Test
    public void readFile() throws IOException {
        String expected = "Foo Foo dsds aa";
        String actual = reader.readFile();
        assertEquals(expected, actual);
    }

    @Test(expectedExceptions = ComparisonFailure.class)
    public void readFileIncorrect() throws IOException {
        String expected = "Foo Foo";
        String actual = reader.readFile();
        assertEquals(expected, actual);
    }

    @Test
    public void stringToList() {
        assertEquals(listStringTest, reader.stringToList(stringTest));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void stringToListIncorrect() {
        List<String> expected = Arrays.asList("Foo", "dsds", "aa");
        assertEquals(expected, reader.stringToList(stringTest));
    }
}